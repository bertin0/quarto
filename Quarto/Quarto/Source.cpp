#include <iostream>

#include "Piece.h"
#include "Board.h"

int main()
{
	Piece piece(Piece::Body::Hollow, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Square);
	Board board; 
	
	std::cout << "Piece example: " << piece << std::endl;
	/*std::cout << "Empty board:\n" << board << std::endl;

	board[{0, 1}] = std::move(piece);
	std::cout << "Moved piece to board:\n" << board << std::endl;*/

	return 0;
}